import React, { useState } from 'react';
import Button from '@mui/material/Button';
import '../App.css';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import Navbar from '../components/Navbar';



const boxStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

// const userButtonStyle = {
//   margin: '10px 30px 10px auto'
// }

const saveButtonStyle = {
  margin: '20px 0 5px'
};

const editButtonStyle = {
  width: '30px',
  margin: '0 0 10px 0',
};

// let nextId = 1;

const Home = () => {

//   const [trigger, setTrigger] = useState(false);
  const [edittrigger, setEdittrigger] = useState(false);
  const [visibleZero, setVisibleZero] = useState(true);
  const [visibleUser, setVisibleUser] = useState(false);
  const [username, setUsername] = useState('');
  const [useraddress, setUseraddress] = useState('');
  const [userhobby, setUserhobby] = useState('');
  const [useridentity, setUseridentity] = useState([]);
  const [search, setSearch] = useState('');
  const [clickedid, setClickedid] = useState(0);
  let passingData = useLocation();

  const navigate = useNavigate();
//   let userData = [];

  if (useridentity.length < passingData.state.id) {
    setUseridentity([
        ...useridentity,
        {
            id: passingData.state.id, 
            name: passingData.state.name, 
            address: passingData.state.address, 
            hobby: passingData.state.hobby
        }

    ]);
    // userData.push({ 
    //     id: passingData.state.id, 
    //     name: passingData.state.name, 
    //     address: passingData.state.address, 
    //     hobby: passingData.state.hobby 
    // });
  }

  if (useridentity.length >= 1) {
    setVisibleZero(false);
    setVisibleUser(true);
  }
  
  const result = useridentity.filter((user) => user.name.toLowerCase().includes(search.toLowerCase()));
  const foundUserById = (ID) => {
    let foundUser = Object.values(useridentity).find(user => user.id === ID);
    return foundUser;
  }

  return (
    <div>

      {/* <div className='navbar'>
        <h3 className='app-name'>My App</h3>
        <Button
          sx={userButtonStyle}
          variant='contained' 
          size='small' 
          className='add-user-button'
          onClick={() => setTrigger(true)}
        >
          Add User
        </Button>
      </div>
      <Modal 
        open={trigger}
        onClose={() => setTrigger(false)}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={boxStyle}>
          <Typography id="modal-modal-title" variant="h6" component="h2" align="center">
            Add User
          </Typography>
          <Box sx={{ width: "100%"}} className='input-box'>
            <label>Name</label>
            <TextField 
              variant='outlined' 
              size='small' 
              // value={username}
              onChange={(e) => {
                setUsername(e.target.value)
              }}/>
            <label>Address</label>
            <TextField 
              variant='outlined' 
              size='small'
              // value={useraddress}
              onChange={(e) => {
                setUseraddress(e.target.value)
              }}/>
            <label>Hobby</label>
            <TextField 
              variant='outlined' 
              size='small'
              // value={userhobby}
              onChange={(e) => {
                setUserhobby(e.target.value)
              }}/>
          </Box>
          <Box sx={{width: "100%" , textAlign: "center"}}>
            <Button
              sx={saveButtonStyle}
              variant='contained' 
              size='small' 
              className='save-button'
              onClick={() => {
                setUsername('');
                setUseraddress('');
                setUserhobby('');
                setUseridentity([
                  ...useridentity,
                  { id: nextId++, name: username, address: useraddress, hobby: userhobby }
                ]);
                console.log(useridentity);
                setVisible1(false);
                setVisible2(true);
              }}
            >
              Save
            </Button>
          </Box>
          
        </Box>
      </Modal> */}
      <Navbar />
      {visibleZero && (
        <div className='no-user'>
          0 USER
        </div>
      )}
      {visibleUser && (
        <div className='user-container'>
          <Box className='search-container'>
            <TextField 
            variant='outlined' size='small' label='Search'
            onChange={(e) => {setSearch(e.target.value)}} />
          </Box>
          {search 
            ? result.map((item) => (
              <div key={item.id} className='user-row'>
                <div className='left-side'>
                  <h2 className='left-content-name'>{item.name}</h2>
                  <p className='left-content-address'>{item.address}</p>
                </div>
                <div className='right-side'>
                  <h3>{item.hobby}</h3>
                  <Link 
                    to={{
                        pathname: `/view/${item.id}`,
                        state: {
                            name: item.name,
                            hobby: item.hobby,
                            address: item.address
                        }
                    }}>
                        <Button 
                            sx={editButtonStyle}
                            variant='contained'
                            size='small'
                            className='view-button'
                            onClick={() => {
                                navigate(`/view/${item.id}`)
                            }}
                            >
                                View
                        </Button>
                  </Link>
                  
                  <Button 
                    sx={editButtonStyle}
                    variant='contained'
                    size='small'
                    className='edit-button'
                    onClick={() => {
                      setEdittrigger(true);
                      setClickedid(item.id);
                      console.log(clickedid);
                      setUsername(foundUserById(clickedid).name);
                      setUserhobby(foundUserById(clickedid).hobby);
                      setUseraddress(foundUserById(clickedid).address);
                      console.log(foundUserById(clickedid).name);
                      console.log(username);
                      console.log(userhobby);
                      console.log(useraddress);
                      // setClickedid(0);
                    }}
                  >
                      Edit
                  </Button>
                </div>
              </div>
            ))
            : useridentity.map((user) => (
              <div key={user.id} className='user-row'>
                <div className='left-side'>
                  <h2 className='left-content-name'>{user.name}</h2>
                  <p className='left-content-address'>{user.address}</p>
                </div>
                <div className='right-side'>
                  <h3>{user.hobby}</h3>
                  <Link 
                    to={{
                        pathname: `/view/${user.id}`,
                        state: {
                            name: user.name,
                            hobby: user.hobby,
                            address: user.address
                        }
                    }}>
                        <Button 
                            sx={editButtonStyle}
                            variant='contained'
                            size='small'
                            className='view-button'
                            onClick={() => {
                                navigate(`/view/${user.id}`)
                            }}
                            >
                                View
                        </Button>
                  </Link>
                  <Button 
                    sx={editButtonStyle}
                    variant='contained'
                    size='small'
                    className='view-button'
                    onClick={() => {
                        navigate("/view")
                    }}
                    >
                        View
                  </Button>
                  <Button 
                    sx={editButtonStyle}
                    variant='contained'
                    size='small'
                    className='edit-button'
                    onClick={() => {
                      setEdittrigger(true);
                      setClickedid(user.id);
                      console.log(clickedid);
                      setUsername(foundUserById(clickedid).name);
                      setUserhobby(foundUserById(clickedid).hobby);
                      setUseraddress(foundUserById(clickedid).address);
                      console.log(foundUserById(clickedid).name);
                      console.log(username);
                      console.log(userhobby);
                      console.log(useraddress);
                      // setClickedid(0);
                    }}
                  >
                      edit
                  </Button>
                </div>
              </div>      
              ))}
          <Modal 
            open={edittrigger}
            onClose={() => {
              setEdittrigger(false);
              setUsername('');
              setUseraddress('');
              setUserhobby('');
            }}
            aria-labelledby='modal-modal-title'
            aria-describedby='modal-modal-description'
          >
            <Box sx={boxStyle}>
              <Typography id="modal-modal-title" variant="h6" component="h2" align="center">
                Edit User
              </Typography>
              <Box sx={{ width: "100%"}} className='input-box'>
                <label>Name</label>
                <TextField 
                  variant='outlined' 
                  size='small' 
                  value={username}
                  // defaultValue=""
                  onChange={(e) => {
                    setUsername(e.target.value)
                  }}/>
                <label>Address</label>
                <TextField 
                  required
                  variant='outlined' 
                  size='small'
                  value={useraddress}
                  onChange={(e) => {
                    setUseraddress(e.target.value)
                  }}/>
                <label>Hobby</label>
                <TextField 
                  variant='outlined' 
                  size='small'
                  value={userhobby}
                  onChange={(e) => {
                    setUserhobby(e.target.value)
                  }}/>
              </Box>
              <Box sx={{width: "100%" , textAlign: "center"}}>
                <Button
                  sx={saveButtonStyle}
                  variant='contained' 
                  size='small' 
                  className='save-button'
                  onClick={() => {
                    setUsername('');
                    setUseraddress('');
                    setUserhobby('');
                    setUseridentity(current =>
                      current.map(obj => {
                        if (obj.id === clickedid) {
                          return {...obj, name: username, address: useraddress, hobby: userhobby};
                        }

                        return obj;
                      })
                      // ...useridentity,
                      // { id: clickedid, name: username, address: useraddress, hobby: userhobby }
                    );
                    setClickedid(0);
                  }}
                >
                  Save
                </Button>
              </Box>
              
            </Box>
          </Modal>
        </div>
      )}
      
    </div>
  )
  
}

export { Home };
