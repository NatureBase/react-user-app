import React from 'react';
import Navbar from "../components/Navbar";
import { useLocation } from 'react-router-dom';


const View = () => {
    const data = useLocation();
    return (
        <div>
        <Navbar />
        <h1>{data.state.name}</h1>
        <h1>{data.state.address}</h1>
        <h1>{data.state.hobby}</h1>
        </div>
    )
}

export { View }
