import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { About } from "./page/About.jsx";
import { Hobby } from "./page/Hobby.jsx";
import { View } from "./page/View.jsx";
import { Home } from "./page/Home.jsx";

const Routers = () => {
  return (
    <Router>
        <Routes>
            <Route exact path='/'>
                <Home />
            </Route>
            <Route exact path='/hobby'>
                <Hobby />
            </Route>
            <Route exact path='/about'>
                <About />
            </Route>
            <Route path='/view:id'>
                <View />
            </Route>
        </Routes>
    </Router>
  )
}

export { Routers };
