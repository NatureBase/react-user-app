import React, { useState } from 'react';
import Button from '@mui/material/Button';

const AddUserButton = (props) => {
  return (
    <div>
      <Button 
      variant='contained' 
      size='small'
      onClick={props}>
        Add User
      </Button>
    </div>
  )
}

export default AddUserButton
