import React, { useState } from 'react';
import Button from '@mui/material/Button';
import '../App.css';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import { NavLink, Link } from 'react-router-dom';

const boxStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
  
const userButtonStyle = {
    margin: '10px 30px 10px auto'
};

const saveButtonStyle = {
    margin: '20px 0 5px'
};

let nextId = 1;

const Navbar = () => {

    const [trigger, setTrigger] = useState(false);
    const [username, setUsername] = useState('');
    const [useraddress, setUseraddress] = useState('');
    const [userhobby, setUserhobby] = useState('');
    // const [useridentity, setUseridentity] = useState([]);
    // const [visible1, setVisible1] = useState(true);
    // const [visible2, setVisible2] = useState(false);  

return (
    <div>
        <div className='navbar'>
            <h3 className='app-name'>My App</h3>
            <NavLink
                to={{
                pathname: "/about"
                }}
                style={({ isActive}) => 
                isActive 
                ? {
                    background: "#4c4cac"
                } 
                : {
                    background: "#202082"
                }}
            >
                    About
            </NavLink>
            <NavLink
                to={{
                pathname: "/hobby"
                }}
                style={({ isActive}) => 
                isActive 
                ? {
                    background: "#4c4cac"
                } 
                : {
                    background: "#202082"
                }}
            >
                    Hobby
            </NavLink>
            <Button
                sx={userButtonStyle}
                variant='contained' 
                size='small' 
                className='add-user-button'
                onClick={() => setTrigger(true)}
            >
                Add User
            </Button>
        </div>
        <Modal 
            open={trigger}
            onClose={() => setTrigger(false)}
            aria-labelledby='modal-modal-title'
            aria-describedby='modal-modal-description'
        >
            <Box sx={boxStyle}>
                <Typography id="modal-modal-title" variant="h6" component="h2" align="center">
                    Add User
                </Typography>
                <Box sx={{ width: "100%"}} className='input-box'>
                    <label>Name</label>
                    <TextField 
                    variant='outlined' 
                    size='small' 
                    // value={username}
                    onChange={(e) => {
                        setUsername(e.target.value)
                    }}/>
                    <label>Address</label>
                    <TextField 
                    variant='outlined' 
                    size='small'
                    // value={useraddress}
                    onChange={(e) => {
                        setUseraddress(e.target.value)
                    }}/>
                    <label>Hobby</label>
                    <TextField 
                    variant='outlined' 
                    size='small'
                    // value={userhobby}
                    onChange={(e) => {
                        setUserhobby(e.target.value)
                    }}/>
                </Box>
                <Box sx={{width: "100%" , textAlign: "center"}}>
                    <Link to={{
                        pathname: "/",
                        state: { 
                            id: nextId++, 
                            name: username, 
                            address: useraddress, 
                            hobby: userhobby
                        }
                    }}>
                    <Button
                        sx={saveButtonStyle}
                        variant='contained' 
                        size='small' 
                        className='save-button'
                        onClick={() => {
                            setUsername('');
                            setUseraddress('');
                            setUserhobby('');
                            // setUseridentity([
                            //     ...useridentity,
                            //     { id: nextId++, name: username, address: useraddress, hobby: userhobby }
                            // ]);
                            // console.log(useridentity);
                            // setVisible1(false);
                            // setVisible2(true);
                        }}
                    >
                        Save
                    </Button>
                    </Link>   
                </Box>
            </Box>
        </Modal>
        
    </div>
    )
}

export default Navbar;

